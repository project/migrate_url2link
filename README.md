# Migrate URL2Link

Provides a migration path for the URL module for Drupal 7 to the Link module in
Drupal 8 and 9.

## Credits / contact

Written and maintained by [Damien
McKenna](https://www.drupal.org/u/damienmckenna).
